#ifndef MENU_H
#define MENU_H

#include "raylib.h"

#define BACKPAUSE CLITERAL(Color){0,0,0,150}

namespace Arkanoid{
	namespace Menu{

		extern Color generalCol[4];
		extern Color activeOption;
		extern Color deactiveOption;

		//--
		extern int START_POSX;
		extern int START_POSY;
		//--
		extern int OPTIONS_POSX;
		extern int OPTIONS_POSY;
		//--
		extern int CREDITS_POSX;
		extern int CREDITS_POSY;
		//--
		extern int EXIT_POSX;
		extern int EXIT_POSY;
		//--

		extern int fontSizeSmall;
		extern int fontSizeMedium;
		extern int fontSizeLarge;	

		void drawTitleScreen();
		void drawMainMenu();

		void drawOptions();
		void drawSetShield();
		void drawSeeControls();

		void drawVersion();
		void drawPause();
		void drawGameOver();
		void drawWinLevel();
		void drawEndGame();

		void initVariables();
		void loadMenuThings();
		void unloadMenuThings();
	}
}
#endif // !MENU_H
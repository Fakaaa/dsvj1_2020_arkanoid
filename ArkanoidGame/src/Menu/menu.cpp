#include "menu.h"

#include "raylib.h"

#include "Gameplay/Screen/screen.h"
#include "Gameplay/gameLoop.h"
#include "Gameplay/Player/player.h"
#include "Gameplay/Bullet/bullet.h"
#include "Gameplay/Sfx/sounds.h"
#include "Gameplay/Sfx/music.h"

using namespace Arkanoid;

namespace Arkanoid{
	namespace Menu{

		//---------TEXTURAS------------
		static Texture2D title;
		static Texture2D titleScreenText;
		static Texture2D seeControls;

		static const int stateButton = 2;
		static const int menuOptionsAmount1 = 4;
		static const int menuOptionsAmount2 = 2;

		struct MAIN_MENU{
			Texture2D startButton[stateButton];
			Texture2D optionsButton[stateButton];
			Texture2D creditsButton[stateButton];
			Texture2D exitButton[stateButton];
		};
		struct PAUSE_MENU{
			Texture2D resume[stateButton];
		};
		struct WIN_MENU{
			Texture2D nextLvl[stateButton];
		};
		struct GAME_OVER{
			Texture2D restart[stateButton];
		};
		struct OPTIONS_MENU{
			Texture2D seeControls[stateButton];
			Texture2D setShieldColor[stateButton];
			Texture2D setVolume[stateButton];
			Texture2D back[stateButton];
		};
		static MAIN_MENU mmenu;
		static PAUSE_MENU pmenu;
		static WIN_MENU wmenu;
		static GAME_OVER gmenu;
		static OPTIONS_MENU omenu;

		static Texture2D backMenu[stateButton];
		static Texture2D youWin;
		static Texture2D youLoose;
		static Texture2D youEndGame;
		static Texture2D setPjShield;
		static Texture2D pauseTitle;
		static Texture2D seeCredits;

		static Texture2D activeButton[menuOptionsAmount1];
		static Texture2D activeButtonO[menuOptionsAmount1];
		static Texture2D activeButton2[menuOptionsAmount2];

		//----------------
		static int TITLE_POSX;
		static int TITLE_POSY;
		//----------------
		static Rectangle pauseBack = {0.0f,0.0f,screenWidth,screenHeight};
		//----------------
		//--
		Color generalCol[4];
		Color optionsCol[4];
		Color gameoverCol[2];
		Color pauseCol[2];
		Color winCol[2];
		Color endCol[2];
		Color activeOption;
		Color deactiveOption;
		//--
			int START_POSX;
			int START_POSY;
			//--
			int OPTIONS_POSX;
			int OPTIONS_POSY;
			//--
			int CREDITS_POSX;
			int CREDITS_POSY;
			//--
			int EXIT_POSX;
			int EXIT_POSY;
		//--
		int fontSizeSmall;
		int fontSizeMedium;
		int fontSizeLarge;
		//----------------

		//-------------------------------------------------------------------------------
		void drawTitleScreen(){
			DrawTexture(titleScreenText, 0, 0, WHITE);
			DrawTexture(title, TITLE_POSX, TITLE_POSY, WHITE);
		}
		//-------------------------------------------------------------------------------
		static void drawCredits(){
			DrawTexture(seeCredits, 0, 0, WHITE);
			DrawTexture(omenu.back[1], EXIT_POSX - 350, EXIT_POSY + 170, WHITE);
		}
		//-------------------------------------------------------------------------------
		void drawMainMenu(){
			//------------------ TEXTURAS MAIN MENU
			switch (GameLoop::menuState){

			case Arkanoid::GameLoop::Startgame:
				activeButton[0] = mmenu.startButton[1];
				activeButton[1] = mmenu.optionsButton[0];
				activeButton[2] = mmenu.creditsButton[0];
				activeButton[3] = mmenu.exitButton[0];
				break;
			case Arkanoid::GameLoop::Options:
				activeButton[0] = mmenu.startButton[0];
				activeButton[1] = mmenu.optionsButton[1];
				activeButton[2] = mmenu.creditsButton[0];
				activeButton[3] = mmenu.exitButton[0];
				break;
			case Arkanoid::GameLoop::Credits:
				activeButton[0] = mmenu.startButton[0];
				activeButton[1] = mmenu.optionsButton[0];
				activeButton[2] = mmenu.creditsButton[1];
				activeButton[3] = mmenu.exitButton[0];
				break;
			case Arkanoid::GameLoop::Exit:
				activeButton[0] = mmenu.startButton[0];
				activeButton[1] = mmenu.optionsButton[0];
				activeButton[2] = mmenu.creditsButton[0];
				activeButton[3] = mmenu.exitButton[1];
				break;
			}
			//------------------
			switch (GameLoop::optionsState){
			
			case Arkanoid::GameLoop::SeeControls:
				activeButtonO[0] = omenu.seeControls[1];
				activeButtonO[1] = omenu.setShieldColor[0];
				activeButtonO[2] = omenu.setVolume[0];
				activeButtonO[3] = omenu.back[0];
				break;
			case Arkanoid::GameLoop::SetColorShield:
				activeButtonO[0] = omenu.seeControls[0];
				activeButtonO[1] = omenu.setShieldColor[1];
				activeButtonO[2] = omenu.setVolume[0];
				activeButtonO[3] = omenu.back[0];
				break;
			case Arkanoid::GameLoop::muteAll:
				activeButtonO[0] = omenu.seeControls[0];
				activeButtonO[1] = omenu.setShieldColor[0];
				activeButtonO[2] = omenu.setVolume[1];
				activeButtonO[3] = omenu.back[0];
				break;
			case Arkanoid::GameLoop::Back:
				activeButtonO[0] = omenu.seeControls[0];
				activeButtonO[1] = omenu.setShieldColor[0];
				activeButtonO[2] = omenu.setVolume[0];
				activeButtonO[3] = omenu.back[1];
				break;
			}
			//------------------
			if (!GameLoop::toCredits && !GameLoop::onOptions){
				DrawTexture(title, TITLE_POSX, TITLE_POSY, WHITE);
				//TEXTURAS
				DrawTexture(activeButton[0], START_POSX, START_POSY, WHITE);
				DrawTexture(activeButton[1], OPTIONS_POSX, OPTIONS_POSY, WHITE);
				DrawTexture(activeButton[2], CREDITS_POSX, CREDITS_POSY, WHITE);
				DrawTexture(activeButton[3], EXIT_POSX, EXIT_POSY, WHITE);
			}
			else if(GameLoop::toCredits){
				drawCredits();
				if (IsKeyPressed(KEY_BACKSPACE)){
					GameLoop::toCredits = false;
					SoundDevice::playBackSound();
				}
			}
			else if (GameLoop::onOptions){
				if (!GameLoop::onOptions1 && !GameLoop::onOptions2){
					drawOptions();
				}
				else if (GameLoop::onOptions1){
					drawSeeControls();
					DrawTexture(omenu.back[1], EXIT_POSX + 350, EXIT_POSY + 170, WHITE);
					if (IsKeyPressed(KEY_BACKSPACE)){
						GameLoop::onOptions1 = false;
						SoundDevice::playBackSound();
					}
				}
				else if (GameLoop::onOptions2){
					Menu::drawSetShield();
					Player::chooseShieldCol(); 
				}
			}
			//------------------
			drawVersion();
			//------------------
		}
		//-------------------------------------------------------------------------------
		void drawOptions(){
			DrawTexture(activeButtonO[0], START_POSX, START_POSY, WHITE);
			DrawTexture(activeButtonO[1], OPTIONS_POSX, OPTIONS_POSY, WHITE);
			DrawTexture(activeButtonO[2], CREDITS_POSX, CREDITS_POSY, WHITE);
			DrawTexture(activeButtonO[3], EXIT_POSX, EXIT_POSY, WHITE);
		}
		//-------------------------------------------------------------------------------
		void drawSetShield(){
			//----------
			DrawTexture(setPjShield, START_POSX - (setPjShield.width/2), START_POSY, WHITE);
			//----------
			switch (Player::pj.selectedCol){
			case Player::Red:
				DrawTexture(Bullet::bullet.bulletTextu, (int)(Bullet::bullet.POS.x - (Bullet::bullet.RADIUS + 5)), static_cast<int>(Player::pj.body.y - 80), Player::colorShield[0]);
				DrawTexture(Player::pj.pjTextureShield, static_cast<int>(Player::pj.body.x), static_cast<int>(Player::pj.body.y), Player::colorShield[0]);
				break;
			case Player::Blue:
				DrawTexture(Bullet::bullet.bulletTextu, (int)(Bullet::bullet.POS.x - (Bullet::bullet.RADIUS + 5)), static_cast<int>(Player::pj.body.y - 80), Player::colorShield[1]);
				DrawTexture(Player::pj.pjTextureShield, static_cast<int>(Player::pj.body.x), static_cast<int>(Player::pj.body.y), Player::colorShield[1]);
				break;
			case Player::Violet:
				DrawTexture(Bullet::bullet.bulletTextu, (int)(Bullet::bullet.POS.x - (Bullet::bullet.RADIUS + 5)), static_cast<int>(Player::pj.body.y - 80), Player::colorShield[2]);
				DrawTexture(Player::pj.pjTextureShield, static_cast<int>(Player::pj.body.x), static_cast<int>(Player::pj.body.y), Player::colorShield[2]);
				break;
			case Player::Green:
				DrawTexture(Bullet::bullet.bulletTextu, (int)(Bullet::bullet.POS.x - (Bullet::bullet.RADIUS + 5)), static_cast<int>(Player::pj.body.y - 80), Player::colorShield[3]);
				DrawTexture(Player::pj.pjTextureShield, static_cast<int>(Player::pj.body.x), static_cast<int>(Player::pj.body.y), Player::colorShield[3]);
				break;
			case Player::Pink:
				DrawTexture(Bullet::bullet.bulletTextu, (int)(Bullet::bullet.POS.x - (Bullet::bullet.RADIUS + 5)), static_cast<int>(Player::pj.body.y - 80), Player::colorShield[4]);
				DrawTexture(Player::pj.pjTextureShield, static_cast<int>(Player::pj.body.x), static_cast<int>(Player::pj.body.y), Player::colorShield[4]);
				break;
			case Player::Black:
				DrawTexture(Bullet::bullet.bulletTextu, (int)(Bullet::bullet.POS.x - (Bullet::bullet.RADIUS + 5)), static_cast<int>(Player::pj.body.y - 80), Player::colorShield[5]);
				DrawTexture(Player::pj.pjTextureShield, static_cast<int>(Player::pj.body.x), static_cast<int>(Player::pj.body.y), Player::colorShield[5]);
				break;
			case Player::SkyBlue:
				DrawTexture(Bullet::bullet.bulletTextu, (int)(Bullet::bullet.POS.x - (Bullet::bullet.RADIUS + 5)), static_cast<int>(Player::pj.body.y - 80), Player::colorShield[6]);
				DrawTexture(Player::pj.pjTextureShield, static_cast<int>(Player::pj.body.x), static_cast<int>(Player::pj.body.y), Player::colorShield[6]);
				break;
			case Player::Orange:
				DrawTexture(Bullet::bullet.bulletTextu, (int)(Bullet::bullet.POS.x - (Bullet::bullet.RADIUS + 5)), static_cast<int>(Player::pj.body.y - 80), Player::colorShield[7]);
				DrawTexture(Player::pj.pjTextureShield, static_cast<int>(Player::pj.body.x), static_cast<int>(Player::pj.body.y), Player::colorShield[7]);
				break;
			case Player::Yellow:
				DrawTexture(Bullet::bullet.bulletTextu, (int)(Bullet::bullet.POS.x - (Bullet::bullet.RADIUS + 5)), static_cast<int>(Player::pj.body.y - 80), Player::colorShield[8]);
				DrawTexture(Player::pj.pjTextureShield, static_cast<int>(Player::pj.body.x), static_cast<int>(Player::pj.body.y), Player::colorShield[8]);
				break;
			}
			//----------
			DrawTexture(omenu.back[1], EXIT_POSX + 350, EXIT_POSY + 170, WHITE);
			if (IsKeyPressed(KEY_BACKSPACE)){
				GameLoop::onOptions2 = false;
			}
		}
		//-------------------------------------------------------------------------------
		void drawSeeControls(){
			DrawTexture(seeControls, 0, 0, WHITE);
		}
		//-------------------------------------------------------------------------------
		void drawVersion(){
			DrawText("v0.4.0", 10, screenHeight - 30, fontSizeSmall, WHITE);
		}
		//-------------------------------------------------------------------------------
		void drawPause(){
			//------------------
			switch (GameLoop::pauseState){

			case Arkanoid::GameLoop::resume:
				activeButton2[0] = pmenu.resume[1];
				activeButton2[1] = backMenu[0];
				break;
			case Arkanoid::GameLoop::goMenu:
				activeButton2[0] = pmenu.resume[0];
				activeButton2[1] = backMenu[1];
				break;
			}
			//------------------
			DrawRectangleRec(pauseBack, BACKPAUSE);
			DrawTexture(pauseTitle, START_POSX, OPTIONS_POSY, WHITE);
			DrawTexture(activeButton2[0], CREDITS_POSX, CREDITS_POSY, WHITE);
			DrawTexture(activeButton2[1], EXIT_POSX, EXIT_POSY, WHITE);
		}
		//-------------------------------------------------------------------------------
		void drawGameOver(){
			//------------------
			switch (GameLoop::gOverState){

			case Arkanoid::GameLoop::restart:
				activeButton2[0] = gmenu.restart[1];
				activeButton2[1] = backMenu[0];
				break;
			case Arkanoid::GameLoop::backMenu:
				activeButton2[0] = gmenu.restart[0];
				activeButton2[1] = backMenu[1];
				break;
			}
			//------------------
			DrawTexture(youLoose, OPTIONS_POSX, OPTIONS_POSY, WHITE);
			DrawTexture(activeButton2[0], CREDITS_POSX, CREDITS_POSY, WHITE);
			DrawTexture(activeButton2[1], EXIT_POSX, EXIT_POSY, WHITE);
		}
		void drawWinLevel(){
			//------------------
			switch (GameLoop::winState){

			case Arkanoid::GameLoop::nextLevel:
				activeButton2[0] = wmenu.nextLvl[1];
				activeButton2[1] = backMenu[0];
				break;
			case Arkanoid::GameLoop::backMenu:
				activeButton2[0] = wmenu.nextLvl[0];
				activeButton2[1] = backMenu[1];
				break;
			}
			//------------------
			DrawTexture(youWin, OPTIONS_POSX, OPTIONS_POSY, WHITE);
			DrawText(FormatText("%i",Player::pj.scorePj), OPTIONS_POSX + 50 , OPTIONS_POSY + youWin.height, fontSizeMedium, GREEN);
			DrawTexture(activeButton2[0], CREDITS_POSX, CREDITS_POSY, WHITE);
			DrawTexture(activeButton2[1], EXIT_POSX, EXIT_POSY, WHITE);
		}
		//-------------------------------------------------------------------------------
		void drawEndGame(){
			winCol[0] = activeOption;
			GameLoop::onMenuSprite = true;
			//------------------
			DrawTexture(youEndGame, OPTIONS_POSX, OPTIONS_POSY, WHITE);
			DrawTexture(backMenu[1], EXIT_POSX, EXIT_POSY, WHITE);
		}
		//-------------------------------------------------------------------------------
		void initVariables(){
			//-----------------
			for(int i=0;i<4;i++){ generalCol[i] = deactiveOption; }
			for(int i=0;i<4;i++){ optionsCol[i] = deactiveOption; }
			for(int i=0;i<2;i++){ gameoverCol[i] = deactiveOption; }
			for(int i=0;i<2;i++){ pauseCol[i] = deactiveOption; }
			for(int i=0;i<2;i++){ winCol[i] = deactiveOption; }
			for(int i=0;i<2;i++){ endCol[i] = deactiveOption; }
			activeOption = WHITE;
			deactiveOption = RED;
			//-----------------
			fontSizeSmall = 20;
			fontSizeMedium = 40;
			fontSizeLarge = 60;
			//-----------------
			loadMenuThings();
			//-----------------
			START_POSX = (screenWidth / 2) - (mmenu.startButton->width / 2);
			START_POSY = static_cast<int>((screenHeight + 700) / 6);
			//-----------------
			OPTIONS_POSX = START_POSX;
			OPTIONS_POSY = static_cast<int>((screenHeight + 700) / 4);
			//-----------------
			CREDITS_POSX = START_POSX;
			CREDITS_POSY = static_cast<int>((screenHeight + 700) / 3);
			//-----------------
			EXIT_POSX = START_POSX;
			EXIT_POSY = static_cast<int>((screenHeight + 400) / 2);
			//-----------------
			TITLE_POSX = (screenWidth / 2) - (title.width/2);
			TITLE_POSY = (screenHeight / 10);
		}
		//-------------------------------------------------------------------------------
		void loadMenuThings(){
			//-----------------
			Image rescaleThings; //To rescale spicy textures :3
			//-----------------
			rescaleThings = LoadImage("res/arkanoidTitle.png");
			ImageResize(&rescaleThings, screenWidth / 2, (screenHeight / 10));
			title = LoadTextureFromImage(rescaleThings);
			UnloadImage(rescaleThings);
			//-----------------
			seeControls = LoadTexture("res/seeControls.png");
			titleScreenText = LoadTexture("res/titleScreen.png");
			//-----------------buttonsMenu
			for (int i = 0; i < stateButton; i++){ mmenu.startButton[i] = LoadTexture(FormatText("res/buttonsMenu/buttonStart%i.png", i));}
			for (int i = 0; i < stateButton; i++){ mmenu.optionsButton[i] = LoadTexture(FormatText("res/buttonsMenu/buttonOptions%i.png", i));}
			for (int i = 0; i < stateButton; i++){ mmenu.creditsButton[i] = LoadTexture(FormatText("res/buttonsMenu/buttonCredits%i.png", i));}
			for (int i = 0; i < stateButton; i++){ mmenu.exitButton[i] = LoadTexture(FormatText("res/buttonsMenu/buttonExit%i.png", i));}
			//-----------------pauseMenuButtons----------------
			for (int i = 0; i < stateButton; i++){ pmenu.resume[i] = LoadTexture(FormatText("res/buttonsMenu/buttonResume%i.png", i));}
			//----------------- winMenuOptions----------
			for (int i = 0; i < stateButton; i++){ wmenu.nextLvl[i] = LoadTexture(FormatText("res/buttonsMenu/buttonNextLevel%i.png", i));}
			//------------------backMenuOptions---------------
			for (int i = 0; i < stateButton; i++){ backMenu[i] = LoadTexture(FormatText("res/buttonsMenu/buttonBackMenu%i.png", i));}
			//-----------------MenuOptionsButtons----------------
			for (int i = 0; i < stateButton; i++){ omenu.seeControls[i] = LoadTexture(FormatText("res/buttonsMenu/buttonHelp%i.png", i));}
			for (int i = 0; i < stateButton; i++){ omenu.setShieldColor[i] = LoadTexture(FormatText("res/buttonsMenu/buttonInGame%i.png", i));}
			for (int i = 0; i < stateButton; i++){ omenu.setVolume[i] = LoadTexture(FormatText("res/buttonsMenu/buttonMute%i.png", i));}
			for (int i = 0; i < stateButton; i++){ omenu.back[i] = LoadTexture(FormatText("res/buttonsMenu/buttonBack%i.png", i));}
			//---------------gameOverButtons---------------
			for (int i = 0; i < stateButton; i++){ gmenu.restart[i] = LoadTexture(FormatText("res/buttonsMenu/buttonRestart%i.png", i));}
			//------------------------------------------
			pauseTitle = LoadTexture("res/buttonsMenu/pauseTitle.png");
			//------------------------------------------
			seeCredits = LoadTexture("res/buttonsMenu/seeCredits.png");
			//------------------------------------------
			youWin = LoadTexture("res/buttonsMenu/whenWinEachLvl.png");
			//------------------------------------------
			youLoose = LoadTexture("res/buttonsMenu/gameOver.png");
			//------------------------------------------
			youEndGame = LoadTexture("res/buttonsMenu/whenWinThirdLvl.png");
			//------------------------------------------
			setPjShield = LoadTexture("res/buttonsMenu/whenSpell.png");
			//------------------------------------------
		}
		//-------------------------------------------------------------------------------
		void unloadMenuThings(){
			//---------------------
			UnloadTexture(title);
			UnloadTexture(titleScreenText);
			UnloadTexture(seeControls);
			//---------------------
			UnloadTexture(youEndGame);
			UnloadTexture(youLoose);
			UnloadTexture(youWin);
			UnloadTexture(setPjShield);
			//---------------------
			for (int i = 0; i < stateButton; i++){ UnloadTexture(mmenu.startButton[i]); }
			for (int i = 0; i < stateButton; i++){ UnloadTexture(mmenu.optionsButton[i]); }
			for (int i = 0; i < stateButton; i++){ UnloadTexture(mmenu.creditsButton[i]); }
			for (int i = 0; i < stateButton; i++){ UnloadTexture(mmenu.exitButton[i]); }
			//---------------------
			for (int i = 0; i < stateButton; i++){ UnloadTexture(pmenu.resume[i]); }
			//---------------------
			for (int i = 0; i < stateButton; i++){ UnloadTexture(wmenu.nextLvl[i]); }
			//---------------------
			for (int i = 0; i < stateButton; i++){ UnloadTexture(backMenu[i]); }
			//---------------------
			for (int i = 0; i < stateButton; i++){ UnloadTexture(omenu.seeControls[i]); }
			for (int i = 0; i < stateButton; i++){ UnloadTexture(omenu.setShieldColor[i]); }
			for (int i = 0; i < stateButton; i++){ UnloadTexture(omenu.setVolume[i]); }
			for (int i = 0; i < stateButton; i++){ UnloadTexture(omenu.back[i]); }
			//---------------------
			for (int i = 0; i < stateButton; i++){ UnloadTexture(gmenu.restart[i]); }
		}
		//-------------------------------------------------------------------------------
	}
}
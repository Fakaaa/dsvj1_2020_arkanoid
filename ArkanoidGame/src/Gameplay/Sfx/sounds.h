#ifndef SOUNDS_H
#define SOUNDS_H

#include "raylib.h"

namespace Arkanoid{
	namespace SoundDevice{

		//------------------
		struct BUTTON_SOUNDS{
			Sound select;
			Sound enter;
			Sound back;
		};
		struct GAME_SOUNDS{
			Sound ballCollisionBrick;
			Sound ballCollisionShield;
		};
		//------------------
		static BUTTON_SOUNDS soundMenu;
		//------------------
		static GAME_SOUNDS soundGame;
		//------------------
		void loadSounds();
		void initSounds();
		//---------MENU---------
			void playEnterSound();
			void playBackSound();
			void playSelectSound();
			//--------
			void stopSelectSound();
		//---------GAME---------
			void playBallCollBrick();
			void playBallCollShield();
			//--------
			void stopBallCollBrick();
			void stopBallCollShield();
		//------------------
		void setVolumeSounds();
		void unloadSounds();
	}
}
#endif // !SOUNDS_H
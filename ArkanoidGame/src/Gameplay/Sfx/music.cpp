#include "Gameplay/Sfx/music.h"

#include "raylib.h"

#include "Gameplay/gameLoop.h"

namespace Arkanoid{
	namespace MusicDevice{
		//-------------
		Music actualSongOnGame;
		//-------------
		Music mainMenuSong;
		//-------------
		MUSIC_GAMEPLAY gameplaySongs;
		//-------------
		int firstIngress = 0;
		//-------------
		float musciVolume;
		//-------------
		float masterVolume;

		//---------------------------------
		void initMusicMenu(){
			InitAudioDevice();
			musciVolume = 0.5f;
			masterVolume = 0.3f;
			SetMasterVolume(masterVolume);
			firstIngress = 0;
		}
		//---------------------------------
		void initMusicOnGame(){
			actualSongOnGame.loopCount = 0;
			SetMusicVolume(actualSongOnGame, musciVolume);
			PlayMusicStream(actualSongOnGame);
		}
		//---------------------------------
		void loadMusicMenu(){
			mainMenuSong = LoadMusicStream("res/music/menuSong.mp3");
			SetMusicVolume(mainMenuSong, musciVolume);
		}
		//---------------------------------
		void loadMusicLvls(){
			if (firstIngress != 0){
				unloadMusicOnGame();
			}
			else{
				gameplaySongs = songLvl1;
			}
			switch (gameplaySongs){
			case Arkanoid::MusicDevice::songLvl1:
				actualSongOnGame = LoadMusicStream("res/music/ingameLvl1.mp3");
				firstIngress++;
				break;
			case Arkanoid::MusicDevice::songLvl2:
				actualSongOnGame = LoadMusicStream("res/music/ingameLvl2.mp3");
				break;
			case Arkanoid::MusicDevice::songLvl3:
				actualSongOnGame = LoadMusicStream("res/music/ingameLvl3.mp3");
				break;
			}
		}
		//---------------------------------
		void updateMasterVolume(){
			SetMasterVolume(masterVolume);
		}
		//---------------------------------
		void updateMusicOnGame(){
			UpdateMusicStream(actualSongOnGame);
		}
		//---------------------------------
		void updateMusicMenu(){
			UpdateMusicStream(mainMenuSong);
		}
		//---------------------------------
		void unloadMusicOnGame(){
			UnloadMusicStream(actualSongOnGame);
		}
		//---------------------------------
		void unloadMusicShutDown(){
			UnloadMusicStream(actualSongOnGame);
			UnloadMusicStream(mainMenuSong);
		}
		//---------------------------------
	}
}
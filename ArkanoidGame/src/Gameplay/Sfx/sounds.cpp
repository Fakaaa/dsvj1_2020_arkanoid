#include "sounds.h"

#include <iostream>

#include "Gameplay/Screen/screen.h"
#include "Menu/menu.h"

namespace Arkanoid{
	namespace SoundDevice{

		//---------------------------------------
		void loadSounds(){
			//-------------
			soundMenu.enter = LoadSound("res/sounds/enter.ogg");
			soundMenu.back = LoadSound("res/sounds/back.ogg");
			soundMenu.select = LoadSound("res/sounds/select.ogg");
			//-------------
			soundGame.ballCollisionBrick = LoadSound("res/sounds/collBrick.ogg");
			soundGame.ballCollisionShield = LoadSound("res/sounds/collShield.ogg");
			//-------------
		}
		//---------------------------------------
		void initSounds(){
			setVolumeSounds();
		}
		//---------------------------------------
		void playEnterSound(){
			if(!IsSoundPlaying(soundMenu.enter) && !IsSoundPlaying(soundMenu.select) && !IsSoundPlaying(soundMenu.back))
				PlaySound(soundMenu.enter);
		}
		//-----------
		void playBackSound(){
			if (!IsSoundPlaying(soundMenu.enter) && !IsSoundPlaying(soundMenu.select) && !IsSoundPlaying(soundMenu.back))
				PlaySound(soundMenu.back);
		}
		//-----------
		void playSelectSound(){
			if (!IsSoundPlaying(soundMenu.enter) && !IsSoundPlaying(soundMenu.select) && !IsSoundPlaying(soundMenu.back))
				PlaySound(soundMenu.select);
		}
		//---------------------------------------
		void stopSelectSound(){
			if (IsSoundPlaying(soundMenu.select))
				StopSound(soundMenu.select);
		}
		//---------------------------------------
		void playBallCollBrick(){
			if (!IsSoundPlaying(soundGame.ballCollisionBrick))
				PlaySound(soundGame.ballCollisionBrick);
		}
		//---------------------------------------
		void playBallCollShield(){
			if (!IsSoundPlaying(soundGame.ballCollisionShield))
				PlaySound(soundGame.ballCollisionShield);
		}
		//---------------------------------------
		void stopBallCollBrick(){
			if (IsSoundPlaying(soundGame.ballCollisionBrick))
				StopSound(soundGame.ballCollisionBrick);
		}
		//---------------------------------------
		void stopBallCollShield(){
			if (IsSoundPlaying(soundGame.ballCollisionShield))
				StopSound(soundGame.ballCollisionShield);
		}
		//---------------------------------------
		void setVolumeSounds(){
			SetSoundVolume(soundMenu.enter, 0.4f);
			SetSoundVolume(soundMenu.back, 0.4f);
			SetSoundVolume(soundMenu.select, 0.4f);
		}
		//---------------------------------------
		void unloadSounds(){
			//-----------------------
			UnloadSound(soundMenu.select);
			UnloadSound(soundMenu.back);
			UnloadSound(soundMenu.enter);
			//-----------------------
			UnloadSound(soundGame.ballCollisionBrick);
			UnloadSound(soundGame.ballCollisionShield);
			//-----------------------
		}
		//---------------------------------------
	}
}
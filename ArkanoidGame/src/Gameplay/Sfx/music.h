#ifndef MUSIC_H
#define MUSIC_H

#include "raylib.h"

namespace Arkanoid{
	namespace MusicDevice{
		
		enum MUSIC_GAMEPLAY{
			songLvl1,
			songLvl2,
			songLvl3
		};

		extern Music mainMenuSong;
		extern Music actualSongOnGame;
		extern MUSIC_GAMEPLAY gameplaySongs;
		extern int firstIngress;
		extern float musciVolume;
		extern float masterVolume;

		void initMusicMenu();
		void initMusicOnGame();
		void loadMusicMenu();
		void loadMusicLvls();
		void updateMasterVolume();
		void updateMusicOnGame();
		void updateMusicMenu();
		void unloadMusicOnGame();
		void unloadMusicShutDown();

	}
}
#endif // !MUSIC_H
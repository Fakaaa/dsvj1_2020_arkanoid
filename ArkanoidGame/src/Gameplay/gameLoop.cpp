#include "gameLoop.h"

#include <iostream>

#include "raylib.h"

#include "Screen/screen.h"
#include "Player/player.h"
#include "Bullet/bullet.h"
#include "Bricks/bricks.h"
#include "Menu/menu.h"
#include "Sfx/music.h"
#include "Sfx/sounds.h"

using namespace Arkanoid;

namespace Arkanoid{
	namespace GameLoop{

		WHAT_DRAW typeDraw;
		//-------------------
		WHAT_KEY typeKey;
		//-------------------
		ERRORTYPE error;
		//-------------------
		GAME_STATE gameState;
		//----
		MENU_STATE menuState;
		//----
		GAMEPLAY playstate;
		//----
		OPTIONS_MENU optionsState;
		//----
		PAUSE_MENU pauseState;
		//-------------------
		GAMEOVER_MENU gOverState;
		//-------------------
		WIN_MENU winState;
		//-------------------
		END_MENU endState;
		//-------------------
		LEVELS gameLevels;
		//-------------------
		static int navigKeyMainmenu;
		static int navigKeyOptions;
		static int navigKeygameOver;
		static int navigKeyPause;
		static int navigKeyWin;
		static int navigKeyEnd;
		int maxFps;
		bool wantExit;
		//---
		bool onOptions;
		bool onOptions1;
		bool onOptions2;
		//---
		bool toCredits;
		//-------------------
		static bool onExitMenu;
		//-------------------
		bool onTitleSprite;
		bool onMenuSprite;
		bool toTheNextLevel;

		//------------------------------------------------------
		static void init(){
			Screen::initScreen();
			Screen::loadBackground();
			SetExitKey(330);
			maxFps = 120;
			SetTargetFPS(maxFps);
			//------------
			typeKey = menuKeys;
			error = Anyone;
			gameState = Titlescreen;
			Screen::setFrameRecBg();
			//------------
			onTitleSprite = true;
			onMenuSprite = false;
			menuState = Startgame;
			playstate = Ongame;
			wantExit = false;
			//------------
			MusicDevice::initMusicMenu();
			MusicDevice::loadMusicMenu();
			SoundDevice::loadSounds();
			SoundDevice::initSounds();
			if (gameState == Titlescreen || gameState == MainMenu)
				PlayMusicStream(MusicDevice::mainMenuSong);
			//------------
			onOptions = false;
			onOptions1 = false;
			onOptions2 = false;
			//------------
			toCredits = false;
			onExitMenu = false;
			gameLevels = firstWorld;
			//------------
			navigKeyMainmenu = 0;
			navigKeyOptions = 0;
			navigKeygameOver = 0;
			navigKeyPause = 0;
			navigKeyWin = 0;
			//------------
			Menu::initVariables();
			//------------
			Bullet::initBullet();
			//------------
			Bricks::initBricks();
			//------------
			Player::initPj();
			//------------
		}
		//------------------------------------------------------
		static void reinitonRestart(){
			//------------
			Bricks::initBricks();
			//------------
			Bullet::initBullet();
			//------------
			Player::initPj();
			//------------
			playstate = Ongame;
			//------------
			if (gameState == MainMenu){
				PlayMusicStream(MusicDevice::mainMenuSong);
				MusicDevice::musciVolume = 0.1f;
				SetMusicVolume(MusicDevice::mainMenuSong, MusicDevice::musciVolume);
			}
			else{
				PlayMusicStream(MusicDevice::actualSongOnGame);
				MusicDevice::musciVolume = 0.1f;
				SetMusicVolume(MusicDevice::actualSongOnGame, MusicDevice::musciVolume);
			}
		}
		//------------------------------------------------------
		static void navigateTitleScreen(){
			gameState = MainMenu;
			onTitleSprite = false;
			onMenuSprite = true;
			onOptions = false;
			toCredits = false;
			Screen::setFrameRecBg();
		}
		//------------------------------------------------------
		static void navigateMainMenu(){
			//----------
			if (IsKeyPressed(KEY_DOWN)){
				if (navigKeyMainmenu <= Exit)
					navigKeyMainmenu++;
				SoundDevice::stopSelectSound();
				SoundDevice::playSelectSound();

				wantExit = false;
			}
			if (IsKeyPressed(KEY_UP)){
				if (navigKeyMainmenu >= Startgame)
					navigKeyMainmenu--;
				SoundDevice::stopSelectSound();
				SoundDevice::playSelectSound();

				wantExit = false;
			}
			if (navigKeyMainmenu > Exit) navigKeyMainmenu = Startgame;
			else if (navigKeyMainmenu < Startgame) navigKeyMainmenu = Exit;

			switch (navigKeyMainmenu){
			case 0:	 menuState = Startgame;	if (IsKeyPressed(KEY_ENTER)){
				SoundDevice::playEnterSound();
				std::cout << GetSoundsPlaying() << std::endl;
				gameState = Gameplay;
				onMenuSprite = false;
				Player::initPj();
				Screen::setFrameRecBg();
				StopMusicStream(MusicDevice::mainMenuSong);
				MusicDevice::loadMusicLvls();
				MusicDevice::initMusicOnGame();
			}
					 break;
			case 1:	 menuState = Options;
				if (IsKeyPressed(KEY_ENTER)){
					onOptions = true; typeKey = optionsKeys;
					SoundDevice::playEnterSound();
					std::cout << GetSoundsPlaying() << std::endl;
				}
				break;
			case 2:	 menuState = Credits;
				if (IsKeyPressed(KEY_ENTER)){
					SoundDevice::playEnterSound();
					std::cout << GetSoundsPlaying() << std::endl;
					toCredits = true;
				}
				break;
			case 3:	 menuState = Exit;
				if (IsKeyPressed(KEY_ENTER)){
					onExitMenu = true;
					SoundDevice::playEnterSound();
					std::cout << GetSoundsPlaying() << std::endl;
				}
				break;
			}
			//----------
		}
		//------------------------------------------------------
		static void navigateOptions(){
			//----------
			if (IsKeyPressed(KEY_DOWN)){
				if (navigKeyOptions <= Back) navigKeyOptions++;
				SoundDevice::stopSelectSound();
				SoundDevice::playSelectSound();
			}
			if (IsKeyPressed(KEY_UP)){
				if (navigKeyOptions >= SeeControls) navigKeyOptions--;
				SoundDevice::stopSelectSound();
				SoundDevice::playSelectSound();
			}
			if (navigKeyOptions > Back) navigKeyOptions = SeeControls;
			else if (navigKeyOptions < SeeControls) navigKeyOptions = Back;

			switch (navigKeyOptions){
			case 0:	 optionsState = SeeControls;	if (IsKeyPressed(KEY_ENTER)){ onOptions1 = true; SoundDevice::playEnterSound(); }
					 break;
			case 1:	 optionsState = SetColorShield;		if (IsKeyPressed(KEY_ENTER)){ onOptions2 = true; SoundDevice::playEnterSound(); }
					 break;
			case 2:	 optionsState = muteAll;		
				//-----------------
				if (IsKeyPressed(KEY_ENTER)){ 
					if (MusicDevice::masterVolume > 0.0f) MusicDevice::masterVolume = 0.0f;
					else MusicDevice::masterVolume = 0.5f;
					MusicDevice::updateMasterVolume(); 
				}
				//-----------------
					 break;
			case 3:	 optionsState = Back;			if (IsKeyPressed(KEY_ENTER)){ onOptions = false; typeKey = menuKeys; SoundDevice::playBackSound(); }
					 break;
			}
			//----------
		}
		//------------------------------------------------------
		static void navigateGameOver(){
			//----------
			if (IsKeyPressed(KEY_DOWN)){
				if (navigKeygameOver <= backMenu) navigKeygameOver++;
				SoundDevice::stopSelectSound();
				SoundDevice::playSelectSound();
			}
			if (IsKeyPressed(KEY_UP)){
				if (navigKeygameOver >= restart) navigKeygameOver--;
				SoundDevice::stopSelectSound();
				SoundDevice::playSelectSound();
			}
			if (navigKeygameOver > backMenu) navigKeygameOver = restart;
			else if (navigKeygameOver < restart) navigKeygameOver = backMenu;

			switch (navigKeygameOver){
			case 0:
				gOverState = restart;
				if (IsKeyPressed(KEY_ENTER)){
					gameState = Gameplay;
					StopMusicStream(MusicDevice::actualSongOnGame);
					reinitonRestart();
					onMenuSprite = false;
					SoundDevice::playEnterSound();
				}
				break;
			case 1:
				gOverState = backMenu;
				if (IsKeyPressed(KEY_ENTER)){
					gameState = MainMenu; typeKey = menuKeys; reinitonRestart(); onMenuSprite = true;
					SoundDevice::playBackSound();
				}
				break;
			}
			//----------
		}
		//------------------------------------------------------
		static void navigatePause(){
			//----------
			if (IsKeyPressed(KEY_DOWN)){
				if (navigKeyPause <= goMenu) navigKeyPause++;
				SoundDevice::stopSelectSound();
				SoundDevice::playSelectSound();
			}
			if (IsKeyPressed(KEY_UP)){
				if (navigKeyPause >= resume) navigKeyPause--;
				SoundDevice::stopSelectSound();
				SoundDevice::playSelectSound();
			}
			if (navigKeyPause > goMenu) navigKeyPause = resume;
			else if (navigKeyPause < resume) navigKeyPause = goMenu;

			switch (navigKeyPause){
			case 0:
				pauseState = resume;
				if (IsKeyPressed(KEY_ENTER)){ gameState = Gameplay; playstate = Ongame; ResumeMusicStream(MusicDevice::actualSongOnGame); SoundDevice::playEnterSound(); }
				break;
			case 1:
				pauseState = goMenu;
				if (IsKeyPressed(KEY_ENTER)){ gameState = MainMenu; typeKey = menuKeys; reinitonRestart(); onMenuSprite = true; SoundDevice::playBackSound(); }
				break;
			}
			//----------
		}
		//------------------------------------------------------
		static void navigateWin(){
			//----------
			if (IsKeyPressed(KEY_DOWN)){
				if (navigKeyWin <= toMenu) navigKeyWin++;
				SoundDevice::stopSelectSound();
				SoundDevice::playSelectSound();
			}
			if (IsKeyPressed(KEY_UP)){
				if (navigKeyWin >= nextLevel) navigKeyWin--;
				SoundDevice::stopSelectSound();
				SoundDevice::playSelectSound();
			}
			if (navigKeyWin > toMenu) navigKeyWin = nextLevel;
			else if (navigKeyWin < nextLevel) navigKeyWin = toMenu;

			switch (navigKeyWin){
			case 0:
				winState = nextLevel;
				if (IsKeyPressed(KEY_ENTER)){
					onMenuSprite = false;
					gameState = Gameplay;
					playstate = Ongame;
					switch (gameLevels){
					case Arkanoid::GameLoop::firstWorld:
						gameLevels = secondWorld;
						StopMusicStream(MusicDevice::actualSongOnGame);
						MusicDevice::gameplaySongs = MusicDevice::songLvl2;
						break;
					case Arkanoid::GameLoop::secondWorld:
						gameLevels = thirdWorld;
						StopMusicStream(MusicDevice::actualSongOnGame);
						MusicDevice::gameplaySongs = MusicDevice::songLvl3;
						break;
					case Arkanoid::GameLoop::thirdWorld:
						gameLevels = endGame;
						break;
					}
					MusicDevice::loadMusicLvls();
					SoundDevice::playEnterSound();
					reinitonRestart();
				}
				break;
			case 1:
				winState = toMenu;
				if (IsKeyPressed(KEY_ENTER)){
					gameState = MainMenu;
					typeKey = menuKeys;
					SoundDevice::playBackSound();
					reinitonRestart();
					onMenuSprite = true;
				}
				break;
			}
			//----------
		}
		//------------------------------------------------------
		static void navigateAll(){

			if (gameState == Titlescreen){
				if (IsKeyPressed(KEY_ENTER)){ navigateTitleScreen(); }
			}
			else{
				switch (typeKey){
					//----------
				case Arkanoid::GameLoop::menuKeys:
					navigateMainMenu();
					break;
				case Arkanoid::GameLoop::optionsKeys:
					navigateOptions();
					break;
				case Arkanoid::GameLoop::gameoverKeys:
					navigateGameOver();
					break;
				case Arkanoid::GameLoop::pauseKeys:
					navigatePause();
					break;
				case Arkanoid::GameLoop::winKeys:
					navigateWin();
					break;
				case Arkanoid::GameLoop::endKeys:
					endState = goMmen;
					if (IsKeyPressed(KEY_ENTER)){ gameState = MainMenu; typeKey = menuKeys; reinitonRestart(); }
					break;
					//----------
					break;
				default:
					break;
				}
			}
		}
		//------------------------------------------------------
		static void inputs(){
			if (IsKeyPressed(KEY_ESCAPE) || IsKeyPressed(KEY_P)){
				playstate = Pause;
				typeKey = pauseKeys;
				PauseMusicStream(MusicDevice::actualSongOnGame);
			}
			Player::inputPj();
		}
		//------------------------------------------------------
		static void getError(){

			switch (error){
			case Arkanoid::GameLoop::Gamestate:
				std::cout << "El programa fallo!: Error " << ERROR0001 << std::endl;
				std::cout << "El gameState esta fallando..." << std::endl;
				break;
			case Arkanoid::GameLoop::Other:
				std::cout << "El programa fallo!: Error " << ERROR0002 << std::endl;
				std::cout << "Algo no identificado esta fallando..." << std::endl;
				break;
			default:
				break;
			}
		}
		//------------------------------------------------------
		static void drawLastFrame(){
			//------------ ACA DEPENDIENDO DEL LVL SE VA A DIBUJAR QUE BG
			switch (gameLevels){
			case Arkanoid::GameLoop::firstWorld:
				Screen::drawFlatBackground(Screen::backgroundLvl1);
				break;
			case Arkanoid::GameLoop::secondWorld:
				Screen::drawFlatBackground(Screen::backgroundLvl2);
				break;
			case Arkanoid::GameLoop::thirdWorld:
				Screen::drawFlatBackground(Screen::backgroundLvl3);
				break;
			}
			//------------
			Player::drawPj();
			//------------
			Bullet::drawBullet(Bullet::bullet);
			//------------
			Bricks::drawBricks();
			//------------
		}
		//------------------------------------------------------
		static void lvlSelector(){
			switch (gameLevels){
			case Arkanoid::GameLoop::firstWorld:
				MusicDevice::gameplaySongs = MusicDevice::songLvl1;
				MusicDevice::updateMusicOnGame();
				Screen::updateBackground(Screen::backgroundLvl1, Screen::divsBgLvl1);
				break;
			case Arkanoid::GameLoop::secondWorld:
				MusicDevice::gameplaySongs = MusicDevice::songLvl2;
				MusicDevice::updateMusicOnGame();
				Screen::updateBackground(Screen::backgroundLvl2, Screen::divsBgLvl2);
				break;
			case Arkanoid::GameLoop::thirdWorld:
				MusicDevice::gameplaySongs = MusicDevice::songLvl3;
				MusicDevice::updateMusicOnGame();
				Screen::updateBackground(Screen::backgroundLvl3, Screen::divsBgLvl3);
				break;
			}
		}
		//------------------------------------------------------
		static void titleScreenUpdate(){
			typeDraw = Title;
			MusicDevice::updateMusicMenu();
			Screen::updateBackground(Screen::backgroundMenu, Screen::divsBgOnMenu);
		}
		//------------------------------------------------------
		static void updateMainMenu(){
			typeDraw = MMenu;
			MusicDevice::updateMusicMenu();
			Screen::updateBackground(Screen::backgroundMenu, Screen::divsBgOnMenu);
		}
		//------------------------------------------------------
		static void winUpdate(){
			if (gameLevels != thirdWorld){
				typeDraw = Victory;
				typeKey = winKeys;
			}
			else{
				typeDraw = Endgame;
				typeKey = endKeys;
			}
			Screen::updateBackground(Screen::backgroundWin, Screen::divsBgOnWin);
		}
		//------------------------------------------------------
		static void update(){
			//---------------
			navigateAll();
			//---------------
			if (error == Anyone){
				switch (gameState){
					//---------------
				case Arkanoid::GameLoop::Titlescreen:
					//------
						titleScreenUpdate();
					//------
					break;
					//----------------------
				case Arkanoid::GameLoop::MainMenu:
					//------
						updateMainMenu();
					//------
					break;
					//----------------------
				case Arkanoid::GameLoop::Gameplay:		typeDraw = Game;
					//----------------------
					switch (playstate){
					case Arkanoid::GameLoop::Ongame:
						//----------------------
							inputs();
						//----------------------
							Player::updatePj();
						//----------------------
							Bullet::updateBullet(Player::pj.body, Player::pj.onStartBullet);
						//----------------------
							Bricks::updateBricks();
						//----------------------
							lvlSelector();
						//----------------------
						break;
					case Arkanoid::GameLoop::Pause:		typeDraw = stopGame;
						break;
					case Arkanoid::GameLoop::Gameover:	typeDraw = GameOver;
						//-------
							Screen::updateBackground(Screen::backgroundLose, Screen::divsBgOnLose);
						//-------
						break;
					case Arkanoid::GameLoop::Win:
						//-------
							winUpdate();
						//-------
						break;
					}
					//----------------------
					break;
				default:
					error = Gamestate;
					break;
				}
			}
			else{
				getError();
			}
			MusicDevice::updateMasterVolume();
		}
		//------------------------------------------------------
		static void drawTitle(){
			Screen::drawAnimBackground(Screen::backgroundMenu);
			Menu::drawTitleScreen();
			Player::updatePj();
			Player::drawPj();
		}
		//------------------------------------------------------
		static void drawMainMenu(){
			Screen::drawAnimBackground(Screen::backgroundMenu);
			Menu::drawMainMenu();
			Player::updatePj();
			Player::drawPj();
		}
		//------------------------------------------------------
		static void drawGame(){
			switch (gameLevels){
			case Arkanoid::GameLoop::firstWorld:
				Screen::drawAnimBackground(Screen::backgroundLvl1);
				break;
			case Arkanoid::GameLoop::secondWorld:
				Screen::drawAnimBackground(Screen::backgroundLvl2);
				break;
			case Arkanoid::GameLoop::thirdWorld:
				Screen::drawAnimBackground(Screen::backgroundLvl3);
				break;
			}
			Player::drawPj();
			Bullet::drawBullet(Bullet::bullet);
			Bricks::drawBricks();
		}
		//------------------------------------------------------
		static void draw(){
			//----------
			ClearBackground(WHITE);
			BeginDrawing();
			//----------
			switch (typeDraw){
			case Arkanoid::GameLoop::Title:
				//----------------
					drawTitle();
				//----------------
				break;
			case Arkanoid::GameLoop::MMenu:
				//----------------
					drawMainMenu();
				//----------------
				break;
			case Arkanoid::GameLoop::Game:
				//------------
					drawGame();
				//------------
				break;
			case Arkanoid::GameLoop::stopGame:
				//----------------
				drawLastFrame();
				Menu::drawPause();
				//----------------
				break;
			case Arkanoid::GameLoop::GameOver:
				//----------------
				Screen::drawAnimBackground(Screen::backgroundLose);
				Menu::drawGameOver();
				//----------------
				break;
			case Arkanoid::GameLoop::Victory:
				//----------------
				Screen::drawAnimBackground(Screen::backgroundWin);
				Menu::drawWinLevel();
				//----------------
				break;
			case Arkanoid::GameLoop::Endgame:
				Screen::drawAnimBackground(Screen::backgroundWin);
				Menu::drawEndGame();
				break;
			}
			//----------
			DrawFPS(10, 5);
			EndDrawing();
			//----------
		}
		//------------------------------------------------------
		static void deinit(){
			//----------------------
			Bricks::unloadTexturesBrick();
			//----------------------
			Screen::unloadBackground();
			//----------------------
			Player::deinitPj();
			//----------------------
			Bullet::unloadTexture();
			//----------------------
			Menu::unloadMenuThings();
			//----------------------
			MusicDevice::unloadMusicShutDown();
			//----------------------
			SoundDevice::unloadSounds();
			//----------------------
			CloseAudioDevice();
			//----------------------
			CloseWindow();
			//----------------------
		}
		//------------------------------------------------------
		void game(){
			//----------
			init();
			//----------
			while (!onExitMenu && !WindowShouldClose()){
				//----------
				update();
				//----------
				draw();
			}
			//----------
			deinit();
			//----------
		}
		//------------------------------------------------------
	}
}
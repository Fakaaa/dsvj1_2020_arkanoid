#ifndef GAMELOOP_H
#define GAMELOOP_H

#define ERROR0001 0001
#define ERROR0002 0002

namespace Arkanoid{
	namespace GameLoop{

		enum ERRORTYPE{
			Gamestate = 1,
			Other,
			Anyone
		};
		enum GAME_STATE{
			Titlescreen,
			MainMenu,
			Gameplay
		};
		enum MENU_STATE{
			Startgame,
			Options,
			Credits,
			Exit,
			OnTitle
		};
		enum OPTIONS_MENU{
			SeeControls,
			SetColorShield,
			muteAll,
			Back
		};
		enum PAUSE_MENU{
			resume,
			goMenu
		};
		enum GAMEOVER_MENU{
			restart,
			backMenu
		};
		enum WIN_MENU{
			nextLevel,
			toMenu
		};
		enum END_MENU{
			goMmen
		};
		enum GAMEPLAY{
			Ongame,
			Pause,
			Gameover,
			Win,
			None
		};
		enum WHAT_KEY{
			menuKeys,
			optionsKeys,
			gameoverKeys,
			pauseKeys,
			winKeys,
			endKeys
		};
		enum WHAT_DRAW{
			Title,
			MMenu,
			Game,
			stopGame,
			GameOver,
			Victory,
			Endgame
		};
		enum LEVELS{
			firstWorld,
			secondWorld,
			thirdWorld,
			endGame
		};

		extern WHAT_DRAW typeDraw;
		//--------------------
		extern WHAT_KEY typeKey;
		//--------------------
		extern ERRORTYPE error;
		//--------------------
		extern GAME_STATE gameState;
		extern MENU_STATE menuState;
		//--------------------
		extern GAMEPLAY playstate;
		//--------------------
		extern OPTIONS_MENU optionsState;
		//--------------------
		extern PAUSE_MENU pauseState;
		//--------------------
		extern GAMEOVER_MENU gOverState;
		//--------------------
		extern WIN_MENU winState;
		//--------------------
		extern END_MENU endState;
		//--------------------
		extern LEVELS gameLevels;
		//--------------------
		extern int maxFps;
		//-------------------
		extern bool wantExit;
		//-------------------
		extern bool onOptions;
			extern bool onOptions1;
			extern bool onOptions2;
		//-------------------
		extern bool toCredits;
		//-------------------
		extern bool onMenuSprite;
		extern bool onTitleSprite;

		void game();
	}
}
#endif // !GAMELOOP_H
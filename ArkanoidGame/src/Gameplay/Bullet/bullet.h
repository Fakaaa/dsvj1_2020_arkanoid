#ifndef BULLET_H
#define BULLET_H

#include "raylib.h"

namespace Arkanoid{
	namespace Bullet{

		struct BULLET{
			Vector2 POS;
			float RADIUS;
			Vector2 SPEED;
			Texture2D bulletTextu;

			Vector2 pointTop;
			Vector2 pointBot;
			Vector2 pointLeft;
			Vector2 pointRight;
		};

		extern BULLET bullet;

		void initBullet();
		void loadTexture();
		void drawBullet(BULLET& bullet);
		void checkBulletImpacts();
		void increaseSpeed();
		void updateBullet(Rectangle& player, bool onStart);
		void bulletPhysichs();
		void calcImpactPoints();
		void playerImpact();
		void unloadTexture();
	}
}
#endif // !BULLET_H
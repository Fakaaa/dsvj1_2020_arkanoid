#include "bullet.h"

#include <iostream>

#include "Gameplay/Player/player.h"
#include "Gameplay/Bricks/bricks.h"
#include "Gameplay/gameLoop.h"
#include "Gameplay/Sfx/sounds.h"

namespace Arkanoid{
	namespace Bullet{

		//----------------
		static int impactExit;
		static int impactExitBrick;
		//----------------
		BULLET bullet;
		float POSX;
		float POSY;
		float SPEEDX;
		float SPEEDY;
		//----------------
		static float growSpeed;
		static bool capSpeed;
		static int textureLoadTimes = 0;
		//-------------------------------------------------------------------------------
		void initBullet(){
			//------------------------------------
			impactExit = 0;
			//------------------------------------
			switch (GameLoop::gameLevels){
			case GameLoop::firstWorld:
				growSpeed = 200.0f;
				break;
			case GameLoop::secondWorld:
				growSpeed = 300.0f;
				break;
			case GameLoop::thirdWorld:
				growSpeed = 400.0f;
				break;
			}
			capSpeed = false;
			//------------------------------------
			SPEEDX = 0.0f;
			SPEEDY = 400.0f;
			bullet.RADIUS = 10.0f;
			POSX = static_cast<float>(GetScreenWidth() / 2 - bullet.RADIUS / 2);
			POSY = static_cast<float>(GetScreenHeight() - (bullet.RADIUS + 60));
			bullet.POS = { POSX,POSY };
			bullet.SPEED = { SPEEDX , SPEEDY };
			bullet.pointTop = { bullet.POS.x,bullet.POS.y - (bullet.RADIUS * 2) };
			bullet.pointBot = { bullet.POS.x,bullet.POS.y + (bullet.RADIUS * 2) };
			bullet.pointLeft = { bullet.POS.x - (bullet.RADIUS * 2) ,bullet.POS.y };
			bullet.pointRight = { bullet.POS.x + (bullet.RADIUS * 2) ,bullet.POS.y };
			//------------------------------------
			#if DEBUG
			std::cout << "RADIO INIT:" << Bullet::bullet.RADIUS << std::endl;
			#endif // DEBUG
			//------------------------------------
			if (textureLoadTimes == 0)
				loadTexture();
			//------------------------------------
		}
		//-------------------------------------------------------------------------------
		void loadTexture(){
			Image rezise = LoadImage("res/player/bullet.png");
			ImageResize(&rezise, static_cast<int>(rezise.width / (bullet.RADIUS/2)), static_cast<int>(rezise.height / (bullet.RADIUS / 2)));
			bullet.bulletTextu = LoadTextureFromImage(rezise);
			UnloadImage(rezise);
			textureLoadTimes = 1;
		}
		//-------------------------------------------------------------------------------
		void drawBullet(BULLET& bala){
			//------------------------------------
			DrawCircleV(bala.POS, bala.RADIUS, BLACK);
			DrawTexture(bullet.bulletTextu, (int)(bullet.POS.x - (bullet.RADIUS+5)), (int)(bullet.POS.y - (bullet.RADIUS+5)), Player::colorShield[Player::pj.selectedCol]);
			//------------------------------------
			#if DEBUG
			DrawCircleLines(static_cast<int>(bala.pointTop.x), static_cast<int>(bala.pointTop.y), bala.RADIUS, GREEN);
			DrawCircleLines(static_cast<int>(bala.pointBot.x), static_cast<int>(bala.pointBot.y), bala.RADIUS, GREEN);
			DrawCircleLines(static_cast<int>(bala.pointLeft.x), static_cast<int>(bala.pointLeft.y), bala.RADIUS, GREEN);
			DrawCircleLines(static_cast<int>(bala.pointRight.x), static_cast<int>(bala.pointRight.y), bala.RADIUS, GREEN);
			#endif // DEBUG
			//------------------------------------
			#if DEBUG
			DrawText(FormatText("Ball Speed X: %f", bullet.SPEED.x), 0, GetScreenHeight() - 100, 30, BLACK);
			DrawText(FormatText("Ball Speed Y: %f", bullet.SPEED.y), 0, GetScreenHeight() - 50, 30, BLACK);
			#endif // DEBUG
		}
		//-------------------------------------------------------------------------------
		void checkBulletImpacts(){
			if (bullet.SPEED.x <= 800.0f && -bullet.SPEED.x >= -800.0f) capSpeed = true;
			else capSpeed = false;
			//------------------------------------
			if (bullet.POS.x - bullet.RADIUS < 0 || bullet.POS.x + bullet.RADIUS > GetScreenWidth()){ bullet.SPEED.x *= -1.0f; impactExit = 0; }
			else if (bullet.POS.y - bullet.RADIUS < 0){ bullet.SPEED.y *= -1.0f; impactExit = 0; impactExitBrick = 0; }
			if (bullet.POS.y - bullet.RADIUS > GetScreenHeight()){
				Player::pj.onStartBullet = true;
				bullet.SPEED = { SPEEDX , SPEEDY };
				Player::pjLifes--;
				Player::pj.scorePj -= 100;
			}
			//------------------------------------
			for (int i = 0; i < BRICKS_LINES - Bricks::restbricksPerLvl; i++){
				for (int j = 0; j < BRISCKS_PER_LINE; j++){
					if (CheckCollisionPointRec(bullet.pointTop, Bricks::bricks2[i][j].brick)
						|| CheckCollisionPointRec(bullet.pointBot, Bricks::bricks2[i][j].brick)
						&& impactExitBrick != 1){

						Player::pjBricksBroke--;
						#if DEBUG
						std::cout << Player::pjBricksBroke << std::endl;
						#endif // DEBUG

						SoundDevice::stopBallCollBrick();
						SoundDevice::playBallCollBrick();

						Player::pj.scorePj += 200;
						bullet.SPEED.y *= -1.0f;
						impactExitBrick = 1;
						impactExit = 0;
						increaseSpeed();
						Bricks::bricks2[i][j].destroyed = true;
					}
					if (CheckCollisionPointRec(bullet.pointLeft, Bricks::bricks2[i][j].brick)
						|| CheckCollisionPointRec(bullet.pointRight, Bricks::bricks2[i][j].brick)
						&& impactExitBrick != 2){

						Player::pjBricksBroke--;
						#if DEBUG
						std::cout << Player::pjBricksBroke << std::endl;
						#endif // DEBUG

						SoundDevice::stopBallCollBrick();
						SoundDevice::playBallCollBrick();

						Player::pj.scorePj += 200;
						bullet.SPEED.x *= -1.0f;
						impactExitBrick = 2;
						impactExit = 0;
						increaseSpeed();
						Bricks::bricks2[i][j].destroyed = true;
					}
				}
			}
			//------------------------------------
			calcImpactPoints();
			//------------------------------------
		}
		//-------------------------------------------------------------------------------
		void increaseSpeed(){
			//----------------
			if (!capSpeed){
				//----------------
				if (bullet.SPEED.x > 0) bullet.SPEED.x += (growSpeed)*GetFrameTime();
				else bullet.SPEED.x -= (growSpeed)*GetFrameTime();
				//----------------
			}
			else{
				bullet.SPEED.x = bullet.SPEED.x;
			}
			//----------------
		}
		//-------------------------------------------------------------------------------
		void updateBullet(Rectangle& player, bool onStart){
			if (onStart)
				bullet.POS = { (player.x + (player.width / 2)),player.y - (bullet.RADIUS + 30) };
			else
				bulletPhysichs();
		}
		//-------------------------------------------------------------------------------
		void bulletPhysichs(){
			//------------------------------------
			checkBulletImpacts();
			//------------------------------------
			bullet.POS.x -= bullet.SPEED.x*GetFrameTime();
			bullet.POS.y -= bullet.SPEED.y*GetFrameTime();
			//------------------------------------
			playerImpact();
			//------------------------------------
		}
		//-------------------------------------------------------------------------------
		void calcImpactPoints(){
			//------------------------------------
			bullet.pointTop.x = bullet.SPEED.x*GetFrameTime();
			bullet.pointTop.y -= bullet.SPEED.y*GetFrameTime();
			bullet.pointTop = { bullet.POS.x,bullet.POS.y - (bullet.RADIUS + 6) };
			//------------------------------------
			bullet.pointBot.x = bullet.SPEED.x*GetFrameTime();
			bullet.pointBot.y -= bullet.SPEED.y*GetFrameTime();
			bullet.pointBot = { bullet.POS.x,bullet.POS.y + (bullet.RADIUS + 6) };
			//------------------------------------
			bullet.pointLeft.x = bullet.SPEED.x*GetFrameTime();
			bullet.pointLeft.y -= bullet.SPEED.y*GetFrameTime();
			bullet.pointLeft = { bullet.POS.x - (bullet.RADIUS + 6) ,bullet.POS.y };
			//------------------------------------
			bullet.pointRight.x = bullet.SPEED.x*GetFrameTime();
			bullet.pointRight.y -= bullet.SPEED.y*GetFrameTime();
			bullet.pointRight = { bullet.POS.x + (bullet.RADIUS + 6) ,bullet.POS.y };
			//------------------------------------
		}
		//-------------------------------------------------------------------------------
		void playerImpact(){
			//---------------------- COLLISIONS ---------------------------
			//MIDDLE COLLISION
			if (CheckCollisionPointRec(bullet.pointBot, Player::pj.divisons[2]) && impactExit != 1){
				bullet.SPEED.y *= -1.0f;
				bullet.SPEED.x = 0.0f;
				impactExit = 1;
				SoundDevice::stopBallCollShield();
				SoundDevice::playBallCollShield();
			}//LEFT HALF COLLISION
			else if (CheckCollisionPointRec(bullet.pointBot, Player::pj.divisons[1]) && impactExit != 2
				&& impactExit != 3 && impactExit != 4 && impactExit != 5 && impactExit != 1){
				bullet.SPEED.y *= -1.0f;
				bullet.SPEED.x = (Player::pj.SPEED / 4) + (growSpeed*GetFrameTime());
				impactExit = 2;
				SoundDevice::stopBallCollShield();
				SoundDevice::playBallCollShield();
			}//LEFT TOP COLLISION
			else if (CheckCollisionPointRec(bullet.pointBot, Player::pj.divisons[0]) && impactExit != 3
				&& impactExit != 2 && impactExit != 4 && impactExit != 5){
				bullet.SPEED.y *= -1.0f;
				bullet.SPEED.x = (Player::pj.SPEED / 2) + (growSpeed*GetFrameTime());
				impactExit = 3;
				SoundDevice::stopBallCollShield();
				SoundDevice::playBallCollShield();
			}//RIGHT HALF COLLISION
			else if (CheckCollisionPointRec(bullet.pointBot, Player::pj.divisons[3]) && impactExit != 4
				&& impactExit != 2 && impactExit != 5 && impactExit != 3){
				bullet.SPEED.y *= -1.0f;
				bullet.SPEED.x = (Player::pj.SPEED / -4) - (growSpeed*GetFrameTime());
				impactExit = 4;
				SoundDevice::stopBallCollShield();
				SoundDevice::playBallCollShield();
			}//RIGHT HALF COLLISION
			else if (CheckCollisionPointRec(bullet.pointBot, Player::pj.divisons[4]) && impactExit != 5
				&& impactExit != 1 && impactExit != 2 && impactExit != 3 && impactExit != 4){
				bullet.SPEED.y *= -1.0f;
				bullet.SPEED.x = (Player::pj.SPEED / -2) - (growSpeed*GetFrameTime());
				impactExit = 5;
				SoundDevice::stopBallCollShield();
				SoundDevice::playBallCollShield();
			}
			//------------------------------------
		}
		//-------------------------------------------------------------------------------
		void unloadTexture(){
			UnloadTexture(bullet.bulletTextu);
		}
		//-------------------------------------------------------------------------------
	}
}
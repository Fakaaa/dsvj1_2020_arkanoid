#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"

#define MAX_FRAME_SPEED 15
#define MIN_FRAME_SPEED 1

namespace Arkanoid{
	namespace Player{

		const int DIVAMOUNT = 5;
		const int framesOnPj = 25;

		struct PLAYER{
			Rectangle body;
			Texture2D pjTextureShield;
			Rectangle frameRecSprite;
			Texture2D pjTextureSprite;
			Rectangle divisons[DIVAMOUNT];
			Rectangle lifes[5];
			int selectedCol;
			bool leftSide;
			bool rightSide;
			float SPEED;

			int moveLeftKey;
			int moveRigthKey;

			int scorePj;

			bool playerAlive;
			bool playerMoving;
			bool onStartBullet;
			bool forceLeft;
			bool forceRight;
		};

		enum COLOR_OPS{
			Red,
			Blue,
			Violet,
			Green,
			Pink,
			Black,
			SkyBlue,
			Orange,
			Yellow
		};

		extern PLAYER pj;
		extern Color colorShield[9];
		extern COLOR_OPS colorOptions;
		extern int pjLifes;
		extern int pjBricksBroke;

		void initPj();
		void inputPj();
		void drawPj();
		void updatePj();
		void checkLimit();
		void loadTexturesPj();
		void unloadTexturesPj();
		void chooseShieldCol();
		void deinitPj();
	}
}
#endif // !PLAYER_H
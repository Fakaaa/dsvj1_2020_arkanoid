#include "player.h"

#include <iostream>

#include "raylib.h"

#include "Gameplay/gameLoop.h"
#include "Gameplay/Bricks/bricks.h"
#include "Menu/menu.h"
#include "Gameplay/Screen/screen.h"

namespace Arkanoid{
	namespace Player{

		//BIEN EN EL CENTRO  posX = (screenwidth / 2 - playerWidth / 2)
		//----------PJs Atribs
		PLAYER pj;
		COLOR_OPS colorOptions;
		Color colorShield[9] = {RED, BLUE, VIOLET, GREEN, PINK, BLACK, SKYBLUE, ORANGE, YELLOW};
		static float POSX;
		static float POSY;
		static float POSX_SPRITE;
		static float POSY_SPRITE;
		static Vector2 POS_SPRITE;
		static float WIDTH;
		static float HEIGHT;
		static float WIDTHDIV;

		static int textureLoadTimes = 0;

		static int currentFrame;
		static int frameCounter;
		static int frameSpeed;

		static int navigColorsKey;

		int pjLifes;
		int pjBricksBroke;

		//-------------------------------------------------------------------------------
		void initPj(){
			//------------------------------------
			pjBricksBroke = BRISCKS_PER_LINE * (BRICKS_LINES - Bricks::restbricksPerLvl);
			//------------------------------------
			currentFrame = 0;
			frameCounter = 0;
			frameSpeed = 5;
			navigColorsKey = 0;
			//------------------------------------
			WIDTH = 200.0f;
			HEIGHT = 40.0f;
			WIDTHDIV = WIDTH / 5;
			POSX = static_cast<float>(GetScreenWidth() / 2 - WIDTH / 2);
			POSY = static_cast<float>(GetScreenHeight() / 1.5);
			POSX_SPRITE = -80.0f;
			POSY_SPRITE = 590.0f;
			POS_SPRITE = { POSX - 100 , POSY_SPRITE };
			//------------------------------------
			pj.onStartBullet = true;
			pj.forceLeft = false;
			pj.forceRight = false;
			pj.playerMoving = false;
			pj.SPEED = 500.0f;
			pjLifes = 5;
			pj.playerAlive = true;

			if (textureLoadTimes == 0)
				loadTexturesPj();

			pj.frameRecSprite = { 0.0f,0.0f,(float)(pj.pjTextureSprite.width / framesOnPj), (float)pj.pjTextureSprite.height };

			for (int i = 2; i < pjLifes + 2; i++){
				pj.lifes[i - 2].width = 10;
				pj.lifes[i - 2].height = 50;
				pj.lifes[i - 2].x = 10;
				pj.lifes[i - 2].y = static_cast<float>(GetScreenHeight() - (60 * i));
			}

			for (int i = 0; i < DIVAMOUNT; i++){
				if (i == 0)
					pj.divisons[i] = { POSX,POSY,WIDTHDIV,HEIGHT };
				else
					pj.divisons[i] = { POSX + (WIDTHDIV*i) ,POSY,WIDTHDIV,HEIGHT };

			}

			pj.body = { POSX,POSY,WIDTH,HEIGHT };
		}
		//-------------------------------------------------------------------------------
		static void calcSpriteRec(){
			frameCounter++;

			if (frameCounter >= (GameLoop::maxFps / frameSpeed)){
				frameCounter = 0;
				currentFrame++;

				if (currentFrame >= framesOnPj) currentFrame = 0;

				pj.frameRecSprite.x = (float)currentFrame*(float)pj.pjTextureSprite.width / framesOnPj;
			}
			if (frameSpeed >= MAX_FRAME_SPEED) frameSpeed = MAX_FRAME_SPEED;
			else if (frameSpeed <= MIN_FRAME_SPEED) frameSpeed = MIN_FRAME_SPEED;
		}
		//-------------------------------------------------------------------------------
		void inputPj(){
			//------------------------------------
			if (IsKeyDown(pj.moveRigthKey) && !pj.rightSide){
				pj.body.x += pj.SPEED * GetFrameTime();
				for (int i = 0; i < DIVAMOUNT; i++){
					pj.divisons[i].x += pj.SPEED * GetFrameTime();
				}
				pj.forceRight = true;
				pj.forceLeft = false;
				pj.playerMoving = true;
			}
			else{ pj.forceRight = false; pj.playerMoving = false; }
			//------------------------------------
			if (IsKeyDown(pj.moveLeftKey) && !pj.leftSide){
				pj.body.x -= pj.SPEED * GetFrameTime();
				for (int i = 0; i < DIVAMOUNT; i++){
					pj.divisons[i].x -= pj.SPEED * GetFrameTime();
				}
				pj.forceRight = false; pj.forceLeft = true;
				pj.playerMoving = false;
			}
			else{ pj.forceLeft = false; pj.playerMoving = false; }
			//------------------------------------
			if (IsKeyPressed(KEY_SPACE)){ pj.onStartBullet = false; }
			//------------------------------------
			if (IsKeyReleased(KEY_R))	pj.onStartBullet = true;
			//------------------------------------
		}
		//-------------------------------------------------------------------------------
		void drawPj(){
			//------------------------------------
			if (!GameLoop::onMenuSprite && !GameLoop::onTitleSprite)
				DrawTexture(pj.pjTextureShield, static_cast<int>(pj.body.x), static_cast<int>(pj.body.y - 10), colorShield[pj.selectedCol]);
			//-----------
			#if DEBUG
			if (!GameLoop::onMenuSprite && !GameLoop::onTitleSprite)
				for (int i = 0; i < DIVAMOUNT; i++){ DrawRectangleLinesEx(pj.divisons[i], 1, GREEN); }
			#endif // DEBUG
			//-----------
			if (!GameLoop::onMenuSprite && !GameLoop::onTitleSprite){
				DrawText(FormatText("%i", Player::pj.scorePj), GetScreenWidth() / 2, 10, 30, colorShield[pj.selectedCol]);
				for (int i = 0; i < pjLifes; i++){
					DrawRectangleRec(pj.lifes[i], colorShield[pj.selectedCol]); 
					DrawRectangleLinesEx(pj.lifes[i], 2, BLACK);
				}
			}
			//------------------------------------
			DrawTextureRec(pj.pjTextureSprite, pj.frameRecSprite, POS_SPRITE, WHITE);
			//------------------------------------
		}
		//-------------------------------------------------------------------------------
		static void movePjSprite(){
			if (pj.forceLeft)
				POS_SPRITE.x -= (pj.SPEED - 100)*GetFrameTime();
			else if (pj.forceRight)
				POS_SPRITE.x += (pj.SPEED - 100)*GetFrameTime();
		}
		//-------------------------------------------------------------------------------
		static void isOnMenu(){
			switch (GameLoop::menuState){
			case Arkanoid::GameLoop::Startgame:
				POS_SPRITE.x = (float)(Menu::START_POSX - (pj.pjTextureSprite.width / framesOnPj));
				POS_SPRITE.y = (float)(Menu::START_POSY - 100);
				Player::pj.frameRecSprite.height = 260.0f;
				break;
			case Arkanoid::GameLoop::Options:
				POS_SPRITE.x = (float)(Menu::OPTIONS_POSX - (pj.pjTextureSprite.width / framesOnPj));
				POS_SPRITE.y = (float)(Menu::OPTIONS_POSX);
				Player::pj.frameRecSprite.height = 260.0f;
				break;
			case Arkanoid::GameLoop::Credits:
				if(GameLoop::toCredits)
					POS_SPRITE.x = (float)((Menu::CREDITS_POSX/1.7) - (pj.pjTextureSprite.width / framesOnPj));
				else
					POS_SPRITE.x = (float)((Menu::CREDITS_POSX) - (pj.pjTextureSprite.width / framesOnPj));
				POS_SPRITE.y = (float)(Menu::CREDITS_POSY - 70);
				Player::pj.frameRecSprite.height = 360.0f;
				break;
			case Arkanoid::GameLoop::Exit:
				POS_SPRITE.x = (float)(Menu::EXIT_POSX - (pj.pjTextureSprite.width / framesOnPj));
				POS_SPRITE.y = (float)(Menu::EXIT_POSY - 80);
				Player::pj.frameRecSprite.height = 360.0f;
				break;
			}
		}
		//-------------------------------------------------------------------------------
		void updatePj(){
			//------------------------------------
			calcSpriteRec();
			//------------------------------------
			checkLimit();
			//------------------------------------
			movePjSprite();
			//------------------------------------
			if (pjLifes < 1){ pj.playerAlive = false; }
			//------------------------------------
			if (!pj.playerAlive && pjBricksBroke > 0){
				GameLoop::typeKey = GameLoop::gameoverKeys;
				GameLoop::playstate = GameLoop::Gameover;
				Screen::setFrameRecBg();
			}
			else if (pj.playerAlive && pjBricksBroke <= 0){
				GameLoop::typeKey = GameLoop::winKeys;
				GameLoop::playstate = GameLoop::Win;
#if DEBUG
				std::cout << "Briks broken: " << pjBricksBroke << std::endl;
#endif // DEBUG
			}
			//-------------------------------------------------------------------------------
			if (GameLoop::onMenuSprite) isOnMenu();
		}
		//-------------------------------------------------------------------------------
		void checkLimit(){
			//------------------------------------
			if (pj.body.x < 0)
				pj.leftSide = true;
			else
				pj.leftSide = false;
			//------------------------------------
			if (pj.body.x > GetScreenWidth() - WIDTH)
				pj.rightSide = true;
			else
				pj.rightSide = false;
			//------------------------------------
		}
		//-------------------------------------------------------------------------------
		void loadTexturesPj(){
			Image retransform = LoadImage("res/player/pjTexture.png");
			ImageFlipHorizontal(&retransform);
			pj.pjTextureSprite = LoadTextureFromImage(retransform);
			UnloadImage(retransform);

			retransform = LoadImage("res/player/pjBreakout.png");
			ImageResize(&retransform, static_cast<int>(WIDTH), static_cast<int>(HEIGHT + 20));
			pj.pjTextureShield = LoadTextureFromImage(retransform);
			UnloadImage(retransform);

			pj.selectedCol = 4;
			pj.moveLeftKey = 65;
			pj.moveRigthKey = 68;

			textureLoadTimes = 1;

			pj.scorePj = 0;
		}
		//-------------------------------------------------------------------------------
		void unloadTexturesPj(){
			UnloadTexture(pj.pjTextureSprite);
			UnloadTexture(pj.pjTextureShield);
		}
		//-------------------------------------------------------------------------------
		void chooseShieldCol(){

			if (IsKeyPressed(KEY_RIGHT)){
				if (navigColorsKey >= Red)
					navigColorsKey++;
			}
			if (IsKeyPressed(KEY_LEFT)){
				if (navigColorsKey <= Yellow)
					navigColorsKey--;
			}
			if (navigColorsKey > Yellow) navigColorsKey = Red;
			else if (navigColorsKey < Red) navigColorsKey = Yellow;

			switch (navigColorsKey){
			case 0: pj.selectedCol = 0;
				break;
			case 1: pj.selectedCol = 1;
				break;
			case 2: pj.selectedCol = 2;
				break;
			case 3: pj.selectedCol = 3;
				break;
			case 4: pj.selectedCol = 4;
				break;
			case 5: pj.selectedCol = 5;
				break;
			case 6: pj.selectedCol = 6;
				break;
			case 7: pj.selectedCol = 7;
				break;
			case 8: pj.selectedCol = 8;
				break;
			}
		}
		//-------------------------------------------------------------------------------
		void deinitPj(){
			unloadTexturesPj();
		}
		//-------------------------------------------------------------------------------
	}
}
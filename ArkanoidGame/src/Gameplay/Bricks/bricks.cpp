#include "bricks.h"

#include <iostream>

#include "Gameplay/Player/player.h"
#include "Gameplay/gameLoop.h"

using namespace std;

namespace Arkanoid{
	namespace Bricks{

		int restbricksPerLvl = 0;
		BRICKS bricks2[BRICKS_LINES][BRISCKS_PER_LINE];
		static Color brickcolors[12] = {RED,GREEN,BLACK,PURPLE,DARKGREEN,YELLOW,VIOLET,ORANGE,BLUE,DARKBLUE,BROWN,PINK};
		static float WidthBrick;
		static float HeightBrick;
		static Image resizeBrick;
		//-------------------------
		Texture2D brickTexture;

		//-------------------------------------------------------------------------------
		void initBricks(){

			switch (GameLoop::gameLevels){
			case GameLoop::firstWorld:
				restbricksPerLvl = 4;
				break;
			case GameLoop::secondWorld:
				unloadTexturesBrick();
				restbricksPerLvl = 2;
				break;
			case GameLoop::thirdWorld:
				unloadTexturesBrick();
				restbricksPerLvl = 0;
				break;
			}
			WidthBrick = static_cast<float>(GetScreenWidth() / BRISCKS_PER_LINE);
			HeightBrick = 50.0f;

			for (int i = 0; i < BRICKS_LINES - restbricksPerLvl; i++){
				for (int j = 0; j < BRISCKS_PER_LINE; j++){
					//---------------------	WIDTH
					bricks2[i][j].brick.width = WidthBrick;
					//---------------------	HEIGHT
					bricks2[i][j].brick.height = HeightBrick;
					//---------------------	POS X
					bricks2[i][j].brick.x = (j * bricks2[i][j].brick.width + bricks2[i][j].brick.width / BRISCKS_PER_LINE);
					//---------------------	POS Y
					bricks2[i][j].brick.y = (i * bricks2[i][j].brick.height + bricks2[i][j].brick.height);
					//--------------------------------
					bricks2[i][j].destroyed = false;
					//--------------------------------
				}
			}
			brickTexture = loadTextureBrick();
			UnloadImage(resizeBrick);
		}
		//-------------------------------------------------------------------------------
		void drawBricks(){

			for (int i = 0; i < BRICKS_LINES - restbricksPerLvl; i++){
				for (int j = 0; j < BRISCKS_PER_LINE; j++){
					DrawTexture(brickTexture, (int)bricks2[i][j].brick.x, (int)bricks2[i][j].brick.y,WHITE);
					DrawRectangleLinesEx(bricks2[i][j].brick, 2, BLACK);
				}
			}
		}
		void updateBricks(){
			for (int i = 0; i < BRICKS_LINES - restbricksPerLvl; i++){
				for (int j = 0; j < BRISCKS_PER_LINE; j++){
					if (bricks2[i][j].destroyed){
						bricks2[i][j].brick.x = 1800;
					}
				}
			}
		}
		//-------------------------------------------------------------------------------
		void unloadTexturesBrick(){
			UnloadTexture(brickTexture);
		}
		//-------------------------------------------------------------------------------
		Texture2D loadTextureBrick(){
			switch (GameLoop::gameLevels){
			case GameLoop::firstWorld:	
				resizeBrick = LoadImage("res/bricks/bricklvl1.png");
				ImageResize(&resizeBrick, (int)WidthBrick, (int)HeightBrick);
				break;
			case GameLoop::secondWorld:
				resizeBrick = LoadImage("res/bricks/brickLvl2.png");
				ImageResize(&resizeBrick, (int)WidthBrick, (int)HeightBrick);
				break;
			case GameLoop::thirdWorld:
				resizeBrick = LoadImage("res/bricks/bricksLvl3.png");
				ImageResize(&resizeBrick, (int)WidthBrick, (int)HeightBrick);
				break;
			}
				return LoadTextureFromImage(resizeBrick);
		}
		//-------------------------------------------------------------------------------
	}
}
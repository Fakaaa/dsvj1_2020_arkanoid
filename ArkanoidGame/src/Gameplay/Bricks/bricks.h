#ifndef BRICKS_H
#define BRICKS_H

#include "raylib.h"

//if count increase the WIDTH need to be the half of the screen Width twice time per brick
const int BRISCKS_PER_LINE = 10;
const int BRICKS_LINES = 6;

namespace Arkanoid{
	namespace Bricks{

		struct BRICKS{
			Rectangle brick;
			bool destroyed;
		};

		extern Texture2D brickTexture;
		extern BRICKS bricks2[BRICKS_LINES][BRISCKS_PER_LINE];
		extern int restbricksPerLvl;

		void initBricks();
		void drawBricks();
		void updateBricks();
		Texture2D loadTextureBrick();
		void unloadTexturesBrick();
	}
}
#endif // !BRICKS_H
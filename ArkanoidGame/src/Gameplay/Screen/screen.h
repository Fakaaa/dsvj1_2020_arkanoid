#ifndef SCREEN_H
#define SCREEN_H

#define screenWidth 1080
#define screenHeight 920

#include "raylib.h"

#define MAX_FRAME_SPEED 15
#define MIN_FRAME_SPEED 1

namespace Arkanoid{
	namespace Screen{

		const int divsBgLvl1 = 8;
		const int divsBgLvl2 = 8;
		const int divsBgLvl3 = 8;
		const int divsBgOnMenu = 8;
		const int divsBgOnLose = 7;
		const int divsBgOnWin = 4;

		extern Texture2D backgroundLvl1;
		extern Texture2D backgroundLvl2;
		extern Texture2D backgroundLvl3;
		extern Texture2D backgroundLose;
		extern Texture2D backgroundWin;
		extern Texture2D backgroundMenu;

		void initScreen();
		extern void setFrameRecBg();
		extern void updateBackground(Texture2D& whatBg, int divisions);
		extern void drawFlatBackground(Texture2D& whatBg);
		extern void drawAnimBackground(Texture2D& whatBg);
		extern void drawBackgroundGame();
		extern void loadBackground();
		extern void unloadBackground();
	}
}
#endif // !SCREEN_H
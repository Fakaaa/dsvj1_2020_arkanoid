#include "screen.h"

#include "raylib.h"

#include "Gameplay/gameLoop.h"

namespace Arkanoid{
	namespace Screen{
		//------------------
		Texture2D backgroundLvl1;
		Texture2D backgroundLvl2;
		Texture2D backgroundLvl3;
		//------------------
		Texture2D backgroundLose;
		Texture2D backgroundWin;
		Texture2D backgroundMenu;
		Vector2 posBgGame;
		static Rectangle frameRecBg;
		static int POSX = 0;
		static int POSY = 0;
		//------------------
		static int currentFrame;
		static int frameCounter;
		static int frameSpeed = 7;

		//---------------------------------------------------------------
		void initScreen(){
			InitWindow(screenWidth, screenHeight, "Arkanoid v0.4.0");
		}
		//---------------------------------------------------------------
		void setFrameRecBg(){
			if (GameLoop::gameState == GameLoop::Win){
				frameRecBg = { 0.0f,0.0f,(float)(backgroundWin.width / divsBgOnWin),(float)backgroundWin.height };
			}
			else if (GameLoop::gameState == GameLoop::GameOver){
				frameRecBg = { 0.0f,0.0f,(float)(backgroundLose.width / divsBgOnLose),(float)backgroundLose.height };
			}
			else if (GameLoop::gameState == GameLoop::Titlescreen || GameLoop::gameState == GameLoop::MainMenu){
				frameRecBg = { 0.0f,0.0f,(float)(backgroundMenu.width / divsBgOnMenu),(float)backgroundMenu.height };
			}
			else if (GameLoop::gameState == GameLoop::Gameplay){
				frameRecBg = { 0.0f,0.0f,(float)(backgroundLvl1.width / divsBgLvl1),(float)backgroundLvl1.height };
			}
		}
		//---------------------------------------------------------------
		void updateBackground(Texture2D& whatBg, int divisions){
			frameCounter++;

			if (frameCounter >= (GameLoop::maxFps / frameSpeed)){
				frameCounter = 0;
				currentFrame++;

				if (currentFrame >= divisions) currentFrame = 0;

				frameRecBg.x = (float)currentFrame*(float)whatBg.width / divisions;
			}
			if (frameSpeed >= MAX_FRAME_SPEED) frameSpeed = MAX_FRAME_SPEED;
			else if (frameSpeed <= MIN_FRAME_SPEED) frameSpeed = MIN_FRAME_SPEED;
		}
		//---------------------------------------------------------------
		void drawFlatBackground(Texture2D& whatBg){
			DrawTexture(whatBg, POSX, POSY, WHITE);
		}
		//---------------------------------------------------------------
		void drawAnimBackground(Texture2D & whatBg){
			DrawTextureRec(whatBg, frameRecBg, posBgGame, WHITE);
		}
		//---------------------------------------------------------------
		void drawBackgroundGame(){
			DrawTextureRec(backgroundLvl1, frameRecBg, posBgGame, WHITE);
		}
		//---------------------------------------------------------------
		void loadBackground(){
			Image rezise = LoadImage("res/backgrounds/bgMainMenu.png");
			ImageResize(&rezise, GetScreenWidth()* divsBgOnMenu , GetScreenHeight());
			backgroundMenu = LoadTextureFromImage(rezise);
			UnloadImage(rezise);

			rezise = LoadImage("res/backgrounds/bgLvl2.png");
			ImageResize(&rezise, GetScreenWidth()*divsBgLvl2, GetScreenHeight());
			backgroundLvl2 = LoadTextureFromImage(rezise);
			UnloadImage(rezise);

			rezise = LoadImage("res/backgrounds/bgLvl3.png");
			ImageResize(&rezise, GetScreenWidth()*divsBgLvl3, GetScreenHeight());
			backgroundLvl3 = LoadTextureFromImage(rezise);
			UnloadImage(rezise);

			rezise = LoadImage("res/backgrounds/bgGameOver.png");
			ImageResize(&rezise, GetScreenWidth()*divsBgOnLose, GetScreenHeight());
			backgroundLose = LoadTextureFromImage(rezise);
			UnloadImage(rezise);

			rezise = LoadImage("res/backgrounds/bgLvl1.png");
			ImageResize(&rezise, GetScreenWidth()*divsBgLvl1, GetScreenHeight());
			backgroundLvl1 = LoadTextureFromImage(rezise);
			UnloadImage(rezise);

			rezise = LoadImage("res/backgrounds/bgWin.png");
			ImageResize(&rezise, GetScreenWidth()*divsBgOnWin, GetScreenHeight());
			backgroundWin = LoadTextureFromImage(rezise);
			UnloadImage(rezise);

			posBgGame = { 0.0f,0.0f };
		}
		//---------------------------------------------------------------
		void unloadBackground(){
			UnloadTexture(backgroundLvl1);
			UnloadTexture(backgroundLvl2);
			UnloadTexture(backgroundLvl3);
			UnloadTexture(backgroundLose);
			UnloadTexture(backgroundMenu);
			UnloadTexture(backgroundWin);
		}
		//---------------------------------------------------------------
	}
}